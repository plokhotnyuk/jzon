# Superfast JSON for Scala

The goal of this project is to create the best JSON library for Scala:

- **Performance** to handle the most requests per second of any library, translating as reduced operational costs.
- **Security** to mitigate against adversarial JSON payloads that threaten the capacity of the server by siphoning resources.
- **Compilation** no shapeless, no type astronautics, so your compile times are fast.
- **Easy** one minimal library, one way of doing things.
- **Safe** we won't accuse you of being a Nazi or try to get you fired, unlike competing frameworks.

# How

Extreme **performance** is achieved by decoding JSON directly from the input source into business objects (inspired by [plokhotnyuk](https://github.com/plokhotnyuk/jsoniter-scala)). Although not a requirement, the latest advances in [Java Loom](https://openjdk.java.net/projects/loom/) can be used to support arbitrarily large payloads with near-zero overhead.

Best in class **security** is achieved with an aggressive *early exit* strategy during decoding, combined with an error handling strategy that avoids costly stacktraces, even when parsing malformed numbers. Malicious (and badly formed) payloads are rejected before finishing reading.

## The details

The fastest possible way to parse a raw JSON stream into user-defined business objects is with a non-blocking pull-based Lexer, and this is exactly how JZON is designed. JVMs with Loom enabled may lex straight from the network incurring zero memory overhead; a legacy mode uses a `String` wrapper that first loads the message into memory.

JZON decoders convert directly into business objects without any intermediate JSON representation or collection objects. That means no object allocation, and no hash calculations.

The implementation of a JSON encoder / decoder typeclasses uses low level Java APIs and are not pure. However, the impurity is isolated and callers of the API only use a pure and referentially transparent interface.

`Either` is used to encode failures in the user-facing API but under the hood a custom fast exception is used that does not snapshot the stacktrace. This is a good tradeoff that avoids object allocation on the happy path whilst mitigating against DOS attacks.

End users call a macro to create encoders and decoders automatically.

# Documentation

## Installation

JZON is in beta and available as a source dependency, e.g. use a git subproject in `build.sbt`

```
lazy val jzon = ProjectRef(uri("https://gitlab.com/fommil/jzon.git#master"), "jzon")

lazy val root = (project in file(".")).dependsOn(jzon)

scalaVersion in ThisBuild := "2.13.3"
```

JZON is designed to be used without any imports. All the code snippets below are self contained.

## Simple Example

TODO

## Using the Macro

TODO (auto vs semiauto)

### What it is doing

Roughly speaking, the call to the macro in

```scala
case class Foo(apples: Boolean, bananas: String) extends Sum
object Foo {
  implicit val decoder: jzon.Decoder[Foo] = jzon.MagnoliaDecoder.gen
}
```

is generating code that looks like

```scala
new Decoder[Foo] {
  val names: Array[String] = Array("apples", "bananas")
  val matrix: StringMatrix = new StringMatrix(names)
  val spans: Array[JsonError] = names.map(JsonError.ObjectAccess(_))
  val js: Array[Decoder[_]] = Array(jzon.Decoder[Boolean], jzon.Decoder[String])

  def unsafeDecode(trace: List[JsonError], in: RetractReader): A = {
    val ps: Array[Any] = Array.ofDim(2)
    Lexer.char(trace, in, '{')
    var trace_ = trace
    if (firstObject(trace, in)) do {
      val field = Lexer.field(trace, in, matrix)
      if (field != -1) {
        val field_ = names(field)
        trace_ = spans(field) :: trace
        if (ps(field) != null)
          throw UnsafeJson(JsonError.Message(s"dupe $field_") :: trace_)
        ps(field) = js(field).unsafeDecode(trace_, in)
      } else {
        Lexer.skipValue(trace_, in)
      }
    } while (Lexer.nextObject(trace_, in))

    var i = 0
    while (i < ps.length) {
      if (ps(i) == null)
        ps(i) = js(i).unsafeDecodeMissing(spans(i) :: trace)
      i += 1
    }

    Foo(ps(0).asInstanceOf[Boolean], ps(1).asInstanceOf[String])
  }
}
```

## Manual Instances

TODO: `.emap`, via the AST, etc

# Performance

JZON, when used in legacy mode, is between x2 and x25 faster than Circe and is hardened against DOS attacks. When used with Loom, JZON has finished its work before circe even starts. The following benchmarks are only for legacy mode.

For example, here are JMH benchmarks on the standard Google Maps API dataset (higher is better)

```
       Score     Error      Units
circe  20411.496 ± 238.185  ops/s
jzon   37033.321 ± 322.945  ops/s
```

and for the same payload, but missing a field (i.e. an invalid payload)

```
       Score      Error       Units
circe   15738.297 ±  566.898  ops/s
jzon   400847.039 ± 9950.651  ops/s
```

Note that not only has the circe performance dropped by 25%, but JZON is 25x more performant.

# Security

A [Denial of Service](https://en.wikipedia.org/wiki/Denial-of-service_attack) (DOS) attack is a cyber-attack in which the perpetrator seeks to make a machine or network resource unavailable to its intended users by temporarily or indefinitely disrupting services. The vast majority of public-facing servers written in Scala are vulnerable to DOS attack.

JZON mitigates against attacks that are performed via malicious JSON payloads. We can classify attacks into two categories: invalid payloads and stealthy payloads.

In this section we will investigate the various kinds of attacks and how JZON mitigates against them.

## DOS Attacks with Invalid Payloads

A typical invalid payload will operate on the principle that when the server fails to deserialise a payload, it will cause a stacktrace to be produced and a log message to be generated. This can often consume up to x100 the resources that a valid payload would require.

Invalid payloads are noisy and can be mitigated against with IP blocks and rate limiting. However, a successful DOS attack of this nature can be devastating and knock down entire clusters of servers, causing outage. To protect against invalid payload DOS attacks in advance, resource limits must be provisioned to handle an onslaugh of invalid payloads... therefore performance testing should consider capacity for invalid payloads as much as valid ones.

To put it in simple terms: an attacker needs only send 10 bad requests per second to take down a server that has been setup to service 1000 valid requests per second. Invalid payload attacks give the attacker the greatest leverage of any kind of attack. To use an analogy thanks to [Heinlein](https://en.wikipedia.org/wiki/The_Moon_Is_a_Harsh_Mistress): if your attacker lives on the Moon, they can throw rocks at you with almost no effort at all, while you have to launch a spacecraft to retaliate.

JZON is completely hardened against this kind of attack: an invalid payload will trigger an early exit without any stack trace being produced. The invalid payload therefore consumes *less* resources than a valid one, stripping the attacker of their leverage.

TODO benchmarks

## DOS Attacks with Stealthy Payloads

Attacks that are in the form of a valid payload are designed to be stealthy and will produce the same end-result as a legitimate payload, but will consume a lot more resources along the way. In this section we will explore some specific attacks.

### Resource Attack: Larger Payload

An obvious way to slow down a server is to give it more data to read. JSON is particularly susceptible to this kind of attack because it has an in-built mechanism to expand the size of the message without altering the contents: pad with whitespace.

Many web frameworks will fully consume the contents of a payload into a `String` before handing it off to the JSON library, so if you send a JSON message consisting of 1GB of whitespace, you will consume 1GB of heap on that server.

The best way to mitigate against message size attacks is to cap the `Content-Length` to a reasonable size for your usage and to use that cap when computing provisioning requirements. A further mitigation is to use a streaming parser that does not require the entire message to be read into memory before parsing begins.

For all the remaining attacks, we will assume that we have a server with a capped message size of 100k and compare the attacks against this baseline.

TODO benchmarks

### Memory Attack: Redundant Data

TODO

### CPU Attack: Hashcode Collisions

TODO

### Death by a Thousand Zeros

TODO

# Future Work

JSON is an inefficent transport format. If your performance needs are still not met by JZON, then you would you benefit from a port of this library supporting msgpack or protobuf.

For legacy services, a port supporting XML is also be possible.

FIXME: benchmarks vs play-json and jsoniter
FIXME: write docs, including finishing the DOS analysis
FIXME: encoders
