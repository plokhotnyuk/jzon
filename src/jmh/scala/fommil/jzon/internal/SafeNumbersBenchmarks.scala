package jzon.internal

import org.openjdk.jmh.annotations.{Benchmark, Scope, Setup, State}
import jzon._

// jmh:run -i 5 -wi 5 -f1 -t2 -w1 -r1 SafeNumbersBench.*

/* DESKTOP
1 op = 10,000 String -> number conversions
"Valid" means the String was created by calling .toString on a randomised number
"Invalid" means a randomised String that should not produce a result
"Unsafe" is an unboxed variant
"Fommil" is my implementation
"Stdlib" is using the Scala .toInt / .toFloat / etc

on a fast desktop

[info] Benchmark                                            Mode  Cnt      Score    Error  Units
[info] SafeNumbersBenchBigDecimal.decodeFommilInvalid      thrpt    5   2893.473 ± 71.772  ops/s
[info] SafeNumbersBenchBigDecimal.decodeFommilUnsafeValid  thrpt    5   2336.651 ±  8.160  ops/s
[info] SafeNumbersBenchBigDecimal.decodeFommilValid        thrpt    5   2307.544 ± 14.938  ops/s
[info] SafeNumbersBenchBigDecimal.decodeStdlibInvalid      thrpt    5    191.153 ± 19.192  ops/s
[info] SafeNumbersBenchBigDecimal.decodeStdlibValid        thrpt    5    877.875 ±  4.158  ops/s
[info] SafeNumbersBenchFloat.decodeFommilInvalid           thrpt    5   2951.598 ± 65.632  ops/s
[info] SafeNumbersBenchFloat.decodeFommilUnsafeValid       thrpt    5   4326.105 ± 24.620  ops/s
[info] SafeNumbersBenchFloat.decodeFommilValid             thrpt    5   4078.772 ± 18.642  ops/s
[info] SafeNumbersBenchFloat.decodeScala213Invalid         thrpt    5  18066.153 ± 82.201  ops/s
[info] SafeNumbersBenchFloat.decodeScala213Valid           thrpt    5   1868.414 ±  5.999  ops/s
[info] SafeNumbersBenchFloat.decodeStdlibInvalid           thrpt    5    192.643 ± 19.947  ops/s
[info] SafeNumbersBenchFloat.decodeStdlibValid             thrpt    5   2255.965 ± 11.327  ops/s
[info] SafeNumbersBenchInt.decodeFommilInvalid             thrpt    5   4651.250 ± 20.931  ops/s
[info] SafeNumbersBenchInt.decodeFommilValid               thrpt    5   6812.677 ± 38.288  ops/s
[info] SafeNumbersBenchInt.decodeFommilValidUnsafe         thrpt    5   7747.447 ± 94.377  ops/s
[info] SafeNumbersBenchInt.decodeScala213Invalid           thrpt    5   9147.868 ± 51.399  ops/s
[info] SafeNumbersBenchInt.decodeScala213Valid             thrpt    5   6989.627 ± 39.738  ops/s
[info] SafeNumbersBenchInt.decodeStdlibInvalid             thrpt    5    212.592 ± 31.398  ops/s
[info] SafeNumbersBenchInt.decodeStdlibValid               thrpt    5   7791.804 ± 23.075  ops/s

on an XPS 13 (9350) laptop with Java 14

[info] Benchmark                                            Mode  Cnt     Score     Error  Units
[info] SafeNumbersBenchBigDecimal.decodeFommilInvalid      thrpt    5  1774.937 ±  74.501  ops/s
[info] SafeNumbersBenchBigDecimal.decodeFommilUnsafeValid  thrpt    5   816.437 ±  47.754  ops/s
[info] SafeNumbersBenchBigDecimal.decodeFommilValid        thrpt    5   770.944 ±  17.178  ops/s
[info] SafeNumbersBenchBigDecimal.decodeStdlibInvalid      thrpt    5   122.885 ±  12.600  ops/s
[info] SafeNumbersBenchBigDecimal.decodeStdlibValid        thrpt    5   577.203 ±   9.965  ops/s
[info] SafeNumbersBenchFloat.decodeFommilInvalid           thrpt    5  1778.465 ±  35.684  ops/s
[info] SafeNumbersBenchFloat.decodeFommilUnsafeValid       thrpt    5  2294.180 ±  45.288  ops/s
[info] SafeNumbersBenchFloat.decodeFommilValid             thrpt    5  2187.107 ±  86.212  ops/s
[info] SafeNumbersBenchFloat.decodeStdlibInvalid           thrpt    5   117.897 ±  11.726  ops/s
[info] SafeNumbersBenchFloat.decodeStdlibValid             thrpt    5  1442.317 ±  45.878  ops/s
[info] SafeNumbersBenchInt.decodeFommilInvalid             thrpt    5  3522.018 ± 132.093  ops/s
[info] SafeNumbersBenchInt.decodeFommilValid               thrpt    5  4210.263 ± 112.372  ops/s
[info] SafeNumbersBenchInt.decodeFommilValidUnsafe         thrpt    5  4733.358 ±  81.210  ops/s
[info] SafeNumbersBenchInt.decodeStdlibInvalid             thrpt    5   130.656 ±  15.498  ops/s
[info] SafeNumbersBenchInt.decodeStdlibValid               thrpt    5  4694.200 ± 120.533  ops/s

 */

// jmh:run -i 5 -wi 5 -f1 -t2 -w1 -r1 SafeNumbersBenchInt.*
@State(Scope.Benchmark)
class SafeNumbersBenchInt {

  //@Param(Array("100", "1000"))
  var size: Int = 10000

  // invalid input. e.g. out of range longs
  var invalids: Array[String] = _
  // numbers randomly distributed
  var valids: Array[String] = _

  @Setup
  def setup(): Unit = {
    val safe: Int = 999999999
    val unsafe: Long = 9999999999L

    val r = new scala.util.Random(0L)

    invalids = Array.fill(size) {
      if (r.nextBoolean())
        r.between(Long.MinValue, Int.MinValue.toLong).toString
      else
        r.between(Int.MaxValue.toLong, Long.MaxValue).toString
    }
    valids = Array.fill(size)(r.nextInt().toString)

    // sanity checks
    assert(decodeStdlibInvalid().toList == decodeFommilInvalid().toList)
    assert(decodeStdlibValid().toList == decodeFommilValid().toList)
  }

  def stdlib(s: String): IntOption =
    try IntSome(s.toInt)
    catch {
      case _: java.lang.NumberFormatException => IntNone
    }

  @Benchmark
  def decodeStdlibValid(): Array[IntOption] = valids.map(stdlib)

  @Benchmark
  def decodeScala213Valid(): Array[Option[Int]] = valids.map(_.toIntOption)

  @Benchmark
  def decodeFommilValid(): Array[IntOption] = valids.map(SafeNumbers.int)

  @Benchmark
  def decodeFommilValidUnsafe(): Array[Int] = valids.map(UnsafeNumbers.int)

  @Benchmark
  def decodeStdlibInvalid(): Array[IntOption] = invalids.map(stdlib)

  @Benchmark
  def decodeScala213Invalid(): Array[Option[Int]] = invalids.map(_.toIntOption)

  @Benchmark
  def decodeFommilInvalid(): Array[IntOption] = invalids.map(SafeNumbers.int)

}

// jmh:run -i 5 -wi 5 -f1 -t2 -w1 -r1 SafeNumbersBenchFloat.*
@State(Scope.Benchmark)
class SafeNumbersBenchFloat {
  //@Param(Array("100", "1000"))
  var size: Int = 10000

  var invalids: Array[String] = _
  var valids: Array[String] = _

  @Setup
  def setup(): Unit = {
    val r = new scala.util.Random(0L)

    invalids = Array.fill(size) {
      val s = r.nextString(10)
      stdlib(s) match {
        case FloatNone    => s
        case FloatSome(_) => "wibble"
      }
    }
    valids = Array.fill(size) {
      val e = r.nextInt(40) - 20
      var s = r.nextInt(0x000fffff) // 20 bits of precision
      if (r.nextBoolean()) s = -s
      s"${s}e${e}"
    }

    // sanity checks
    assert(decodeStdlibInvalid().toList == decodeFommilInvalid().toList)
    assert(decodeStdlibValid().toList == decodeFommilValid().toList)
  }

  def stdlib(s: String): FloatOption =
    try FloatSome(s.toFloat)
    catch {
      case _: java.lang.NumberFormatException => FloatNone
    }

  @Benchmark
  def decodeStdlibValid(): Array[FloatOption] = valids.map(stdlib)

  @Benchmark
  def decodeScala213Valid(): Array[Option[Float]] = valids.map(_.toFloatOption)

  @Benchmark
  def decodeFommilValid(): Array[FloatOption] = valids.map(SafeNumbers.float(_))

  @Benchmark
  def decodeFommilUnsafeValid(): Array[Float] =
    valids.map(UnsafeNumbers.float(_, 128))

  @Benchmark
  def decodeStdlibInvalid(): Array[FloatOption] = invalids.map(stdlib)

  @Benchmark
  def decodeScala213Invalid(): Array[Option[Float]] =
    invalids.map(_.toFloatOption)

  @Benchmark
  def decodeFommilInvalid(): Array[FloatOption] =
    invalids.map(SafeNumbers.float(_))

}

// jmh:run -i 5 -wi 5 -f1 -t2 -w1 -r1 SafeNumbersBenchBigDecimal.*
@State(Scope.Benchmark)
class SafeNumbersBenchBigDecimal {
  //@Param(Array("100", "1000"))
  var size: Int = 10000

  var invalids: Array[String] = _
  var valids: Array[String] = _

  @Setup
  def setup(): Unit = {
    val r = new scala.util.Random(0L)

    invalids = Array.fill(size) {
      val s = r.nextString(10)
      stdlib(s) match {
        case None    => s
        case Some(_) => "wibble"
      }
    }
    valids = Array.fill(size) {
      val bytes: Array[Byte] = Array.ofDim(8)
      r.nextBytes(bytes)
      val scale = r.nextInt()
      new java.math.BigDecimal(new java.math.BigInteger(bytes))
        .scaleByPowerOfTen(scale)
        .toString
    }

    // sanity checks
    assert(decodeStdlibInvalid().toList == decodeFommilInvalid().toList)
    assert(decodeStdlibValid().toList == decodeFommilValid().toList)
  }

  def stdlib(s: String): Option[java.math.BigDecimal] =
    try Some(new java.math.BigDecimal(s))
    catch {
      case _: java.lang.NumberFormatException => None
    }

  @Benchmark
  def decodeStdlibValid(): Array[Option[java.math.BigDecimal]] =
    valids.map(stdlib)

  @Benchmark
  def decodeFommilValid(): Array[Option[java.math.BigDecimal]] =
    valids.map(SafeNumbers.bigdecimal(_))

  @Benchmark
  def decodeFommilUnsafeValid(): Array[java.math.BigDecimal] =
    valids.map(UnsafeNumbers.bigdecimal(_, 128))

  @Benchmark
  def decodeStdlibInvalid(): Array[Option[java.math.BigDecimal]] =
    invalids.map(stdlib)

  @Benchmark
  def decodeFommilInvalid(): Array[Option[java.math.BigDecimal]] =
    invalids.map(SafeNumbers.bigdecimal(_))

}
