package jzon

import java.nio.charset.StandardCharsets.UTF_8
import java.util.concurrent.TimeUnit

import com.github.plokhotnyuk.jsoniter_scala.core._
import com.github.plokhotnyuk.jsoniter_scala.macros._
import io.circe
import jzon.TestUtils._
import jzon.data.twitter._
import org.openjdk.jmh.annotations._
import play.api.libs.{json => Play}
import TwitterAPIBenchmarks._

import scala.util.Try

// jmh:run TwitterAPIBenchmarks
//
// jmh:run -i 5 -wi 5 -f1 -t2 -w1 -r1 Twitter.*JzonSuccess2
// jmh:run -i 1 -wi 0 -f0 -t1 -w0 -r60 Twitter.*JzonSuccess2
// see GoogleMapsAPIBenchmarks for profiling instructions

/*
 desktop

[info] Benchmark                                  Mode  Cnt      Score     Error  Units
[info] TwitterAPIBenchmarks.decodeCirceError     thrpt    5  32540.608 ± 165.391  ops/s
[info] TwitterAPIBenchmarks.decodeCirceSuccess1  thrpt    5  32490.016 ± 264.676  ops/s
[info] TwitterAPIBenchmarks.decodeCirceSuccess2  thrpt    5  38683.208 ± 372.213  ops/s
[info] TwitterAPIBenchmarks.decodeJzonError      thrpt    5  34737.184 ± 233.342  ops/s
[info] TwitterAPIBenchmarks.decodeJzonSuccess1   thrpt    5  31746.747 ± 217.746  ops/s
[info] TwitterAPIBenchmarks.decodeJzonSuccess2   thrpt    5  36786.129 ± 290.451  ops/s

 XPS 13 laptop

[info] Benchmark                                  Mode  Cnt      Score      Error  Units
[info] TwitterAPIBenchmarks.decodeCirceError     thrpt    5  19098.916 ± 1083.752  ops/s
[info] TwitterAPIBenchmarks.decodeCirceSuccess1  thrpt    5  17856.209 ±  696.043  ops/s
[info] TwitterAPIBenchmarks.decodeCirceSuccess2  thrpt    5  23529.512 ±  585.099  ops/s
[info] TwitterAPIBenchmarks.decodeJzonError      thrpt    5  20646.358 ±  652.804  ops/s
[info] TwitterAPIBenchmarks.decodeJzonSuccess1   thrpt    5  19069.609 ± 2004.080  ops/s
[info] TwitterAPIBenchmarks.decodeJzonSuccess2   thrpt    5  22109.668 ±  554.634  ops/s
[info] TwitterAPIBenchmarks.decodePlayError      thrpt    5   3618.420 ±  821.890  ops/s
[info] TwitterAPIBenchmarks.decodePlaySuccess1   thrpt    5   3789.612 ± 2038.944  ops/s
[info] TwitterAPIBenchmarks.decodePlaySuccess2   thrpt    5   3696.551 ±  873.797  ops/s

 */

// reference for the format of tweets: https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets.html

@State(Scope.Thread)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(value = 1, jvmArgs = Array(
  "-server",
  "-Xms2g",
  "-Xmx2g",
  "-XX:NewSize=1g",
  "-XX:MaxNewSize=1g",
  "-XX:InitialCodeCacheSize=512m",
  "-XX:ReservedCodeCacheSize=512m",
  "-XX:+UseParallelGC",
  "-XX:-UseAdaptiveSizePolicy",
  "-XX:MaxInlineLevel=18",
  "-XX:-UseBiasedLocking",
  "-XX:+AlwaysPreTouch",
  "-XX:+UseNUMA",
  "-XX:-UseAdaptiveNUMAChunkSizing"
))
class TwitterAPIBenchmarks {
  var jsonString, jsonStringCompact, jsonStringErr: String = _
  var jsonChars, jsonCharsCompact, jsonCharsErr: CharSequence = _

  @Setup
  def setup(): Unit = {
    jsonString = getResourceAsString("twitter_api_response.json")
    jsonChars = asChars(jsonString)
    jsonStringCompact = getResourceAsString("twitter_api_compact_response.json")
    jsonCharsCompact = asChars(jsonStringCompact)
    jsonStringErr = getResourceAsString("twitter_api_error_response.json")
    jsonCharsErr = asChars(jsonStringErr)

    assert(decodeJsoniterScalaSuccess1() == decodeJzonSuccess1())
    assert(decodeJsoniterScalaSuccess2() == decodeJzonSuccess1())
    assert(decodeJsoniterScalaError().isLeft)

    assert(decodeCirceSuccess1() == decodeJzonSuccess1())
    assert(decodeCirceSuccess2() == decodeJzonSuccess2())
    assert(decodeCirceError().isLeft)

    assert(decodePlaySuccess1() == decodeJzonSuccess1())
    assert(decodePlaySuccess2() == decodeJzonSuccess2())
    assert(decodePlayError().isLeft)

    assert(decodeJzonError().isLeft)
  }

  @Benchmark
  def decodeJsoniterScalaSuccess1(): Either[String, List[Tweet]] =
    Try(readFromArray(jsonString.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJsoniterScalaSuccess2(): Either[String, List[Tweet]] =
    Try(readFromArray(jsonStringCompact.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJsoniterScalaError(): Either[String, List[Tweet]] =
    Try(readFromArray(jsonStringErr.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeCirceSuccess1(): Either[circe.Error, List[Tweet]] =
    circe.parser.decode[List[Tweet]](jsonString)

  @Benchmark
  def decodeCirceSuccess2(): Either[circe.Error, List[Tweet]] =
    circe.parser.decode[List[Tweet]](jsonStringCompact)

  @Benchmark
  def decodeCirceError(): Either[circe.Error, List[Tweet]] =
    circe.parser.decode[List[Tweet]](jsonStringErr)

  @Benchmark
  def decodePlaySuccess1(): Either[String, List[Tweet]] =
    Try(Play.Json.parse(jsonString).as[List[Tweet]]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodePlaySuccess2(): Either[String, List[Tweet]] =
    Try(Play.Json.parse(jsonStringCompact).as[List[Tweet]]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodePlayError(): Either[String, List[Tweet]] =
    Try(Play.Json.parse(jsonStringErr).as[List[Tweet]]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJzonSuccess1(): Either[String, List[Tweet]] =
    jzon.parser.decode[List[Tweet]](jsonChars)

  @Benchmark
  def decodeJzonSuccess2(): Either[String, List[Tweet]] =
    jzon.parser.decode[List[Tweet]](jsonCharsCompact)

  @Benchmark
  def decodeJzonError(): Either[String, List[Tweet]] =
    jzon.parser.decode[List[Tweet]](jsonCharsErr)

}

object TwitterAPIBenchmarks {
  // these avoid the List implicit from being recreated every time
  implicit val zListTweet: jzon.Decoder[List[Tweet]] =
    jzon.Decoder.list[Tweet]
  implicit val cListTweet: circe.Decoder[List[Tweet]] =
    circe.Decoder.decodeList[Tweet]
  implicit val codec: JsonValueCodec[List[Tweet]] =
    JsonCodecMaker.make
}
