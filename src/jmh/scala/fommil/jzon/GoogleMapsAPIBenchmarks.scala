package jzon

import java.nio.charset.StandardCharsets.UTF_8
import java.util.concurrent.TimeUnit

import com.github.plokhotnyuk.jsoniter_scala.core._
import com.github.plokhotnyuk.jsoniter_scala.macros._
import io.circe
import jzon.GoogleMapsAPIBenchmarks._
import jzon.TestUtils._
import jzon.data.googlemaps._
import org.openjdk.jmh.annotations._
import play.api.libs.{json => Play}

import scala.util.Try

// jmh:run GoogleMapsAPIBenchmarks
/*
on desktop

[info] Benchmark                                     Mode  Cnt       Score      Error  Units
[info] GoogleMapsAPIBenchmarks.decodeCirceAttack1   thrpt    5    9302.153 ±   46.124  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceAttack2   thrpt    5    9302.725 ±   13.822  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceAttack3   thrpt    5    4065.741 ±   48.007  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceError     thrpt    5   28638.365 ±  148.542  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceSuccess1  thrpt    5   18769.476 ±  351.092  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceSuccess2  thrpt    5   21319.385 ±  208.040  ops/s
[info] GoogleMapsAPIBenchmarks.decodePlayAttack2    thrpt    5    5467.340 ±   42.876  ops/s
[info] GoogleMapsAPIBenchmarks.decodePlayAttack3    thrpt    5    3540.136 ±   26.621  ops/s
[info] GoogleMapsAPIBenchmarks.decodePlayError      thrpt    5   11516.649 ±   66.398  ops/s
[info] GoogleMapsAPIBenchmarks.decodePlaySuccess1   thrpt    5   12138.681 ±   96.225  ops/s
[info] GoogleMapsAPIBenchmarks.decodePlaySuccess2   thrpt    5   13133.487 ±   99.061  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonAttack1    thrpt    5  805849.634 ± 1687.019  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonAttack2    thrpt    5    9393.528 ±   36.677  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonAttack3    thrpt    5   10858.774 ±   61.832  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonError      thrpt    5  793680.220 ± 5819.427  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonSuccess1   thrpt    5   29909.711 ±   66.458  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonSuccess2   thrpt    5   37291.205 ±  125.436  ops/s

on an XPS 13 (9350) laptop with Java 14

[info] Benchmark                                     Mode  Cnt       Score       Error  Units
[info] GoogleMapsAPIBenchmarks.decodeCirceAttack1   thrpt    5    5339.372 ±   121.002  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceAttack2   thrpt    5    5619.680 ±   154.505  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceAttack3   thrpt    5    2738.802 ±    64.223  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceError     thrpt    5   20798.661 ±   824.005  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceSuccess1  thrpt    5   11194.866 ±   478.185  ops/s
[info] GoogleMapsAPIBenchmarks.decodeCirceSuccess2  thrpt    5   12456.125 ±   246.408  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonAttack1    thrpt    5  379332.965 ±  9242.793  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonAttack2    thrpt    5    5528.677 ±   251.513  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonAttack3    thrpt    5    6427.022 ±   298.647  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonError      thrpt    5  365094.570 ± 38993.476  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonSuccess1   thrpt    5   19244.347 ±   546.273  ops/s
[info] GoogleMapsAPIBenchmarks.decodeJzonSuccess2   thrpt    5   23691.713 ±   291.395  ops/s

 */

// To enable the yourkit agent enable a profiling mode, e.g.:
//
// set neoJmhYourkit in Jmh := Seq("sampling")
// set neoJmhYourkit in Jmh := Seq("allocsampled")
//
// more options at https://www.yourkit.com/docs/java/help/startup_options.jsp
//
// When profiling only run one longer test at a time, e.g.
//
// jmh:run -i 1 -wi 0 -f0 -t1 -w0 -r60 GoogleMaps.*decodeJzonSuccess1
//
// and look for the generated snapshot in YourKit (ignore the rest)
//
// Also try the async profiler, e.g.
//
//  jmh:run -i 1 -wi 0 -f1 -t1 -w0 -r60 -prof jmh.extras.Async GoogleMaps.*encodeMagnolia
//  jmh:run -i 1 -wi 0 -f1 -t1 -w0 -r60 -prof jmh.extras.Async:event=alloc GoogleMaps.*encodeMagnolia
//
// which may require kernel permissions:
//
//   echo 1 | sudo tee /proc/sys/kernel/perf_event_paranoid
//   echo 0 | sudo tee /proc/sys/kernel/kptr_restrict
//
// and needs these projects installed, with these variables:
//
// export ASYNC_PROFILER_DIR=$HOME/Projects/async-profiler
// export FLAME_GRAPH_DIR=$HOME/Projects/FlameGraph
//
// http://malaw.ski/2017/12/10/automatic-flamegraph-generation-from-jmh-benchmarks-using-sbt-jmh-extras-plain-java-too/
// (note you need to type `make` in the async-profiler directory)
//
// to use allocation profiling, you need debugging symbols in your jvm. e.g. use
// the Zulu Java distribution.

@State(Scope.Thread)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(value = 1, jvmArgs = Array(
  "-server",
  "-Xms2g",
  "-Xmx2g",
  "-XX:NewSize=1g",
  "-XX:MaxNewSize=1g",
  "-XX:InitialCodeCacheSize=512m",
  "-XX:ReservedCodeCacheSize=512m",
  "-XX:+UseParallelGC",
  "-XX:-UseAdaptiveSizePolicy",
  "-XX:MaxInlineLevel=18",
  "-XX:-UseBiasedLocking",
  "-XX:+AlwaysPreTouch",
  "-XX:+UseNUMA",
  "-XX:-UseAdaptiveNUMAChunkSizing"
))
class GoogleMapsAPIBenchmarks {
  var jsonString, jsonStringCompact, jsonStringErr: String = _
  var jsonStringAttack1, jsonStringAttack2, jsonStringAttack3: String = _
  var jsonChars, jsonCharsCompact, jsonCharsErr: CharSequence = _
  var jsonCharsAttack1, jsonCharsAttack2, jsonCharsAttack3: CharSequence = _

  @Setup
  def setup(): Unit = {
    //Distance Matrix API call for top-10 by population cities in US:
    //https://maps.googleapis.com/maps/api/distancematrix/json?origins=New+York|Los+Angeles|Chicago|Houston|Phoenix+AZ|Philadelphia|San+Antonio|San+Diego|Dallas|San+Jose&destinations=New+York|Los+Angeles|Chicago|Houston|Phoenix+AZ|Philadelphia|San+Antonio|San+Diego|Dallas|San+Jose
    jsonString = getResourceAsString("google_maps_api_response.json")
    jsonChars = asChars(jsonString)
    jsonStringCompact = getResourceAsString(
      "google_maps_api_compact_response.json"
    )
    jsonCharsCompact = asChars(jsonStringCompact)
    jsonStringErr = getResourceAsString("google_maps_api_error_response.json")
    jsonCharsErr = asChars(jsonStringErr)
    jsonStringAttack1 = getResourceAsString("google_maps_api_attack1.json")
    jsonCharsAttack1 = asChars(jsonStringAttack1)
    jsonStringAttack2 = getResourceAsString("google_maps_api_attack2.json")
    jsonCharsAttack2 = asChars(jsonStringAttack2)
    jsonStringAttack3 = getResourceAsString("google_maps_api_attack3.json")
    jsonCharsAttack3 = asChars(jsonStringAttack3)

    assert(decodeJsoniterScalaSuccess1() == decodeJzonSuccess1())
    assert(decodeJsoniterScalaSuccess2() == decodeJzonSuccess2())
    assert(decodeJsoniterScalaError().isLeft)
    assert(decodeJsoniterScalaAttack1().isLeft)
    assert(decodeJsoniterScalaAttack2() == decodeJzonAttack2())
    assert(decodeJsoniterScalaAttack3() == decodeJzonAttack3())

    assert(decodeCirceSuccess1() == decodeJzonSuccess1())
    assert(decodeCirceSuccess2() == decodeJzonSuccess2())
    assert(decodeCirceError().isLeft)
    assert(decodeCirceAttack1().isRight)
    assert(decodeCirceAttack2() == decodeJzonAttack2())
    assert(decodeCirceAttack3() == decodeJzonAttack3())

    assert(decodePlaySuccess1() == decodeJzonSuccess1())
    assert(decodePlaySuccess2() == decodeJzonSuccess2())
    assert(decodePlayError().isLeft)
    assert(decodePlayAttack1().isLeft)
    assert(decodePlayAttack2() == decodeJzonAttack2())
    assert(decodePlayAttack3() == decodeJzonAttack3())

    assert(decodeJzonError().isLeft)
    assert(decodeJzonAttack1().isLeft)
  }

  @Benchmark
  def decodeJsoniterScalaSuccess1(): Either[String, DistanceMatrix] =
    Try(readFromArray(jsonString.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJsoniterScalaSuccess2(): Either[String, DistanceMatrix] =
    Try(readFromArray(jsonStringCompact.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJsoniterScalaError(): Either[String, DistanceMatrix] =
    Try(readFromArray(jsonStringErr.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJsoniterScalaAttack1(): Either[String, DistanceMatrix] =
    Try(readFromArray(jsonStringAttack1.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJsoniterScalaAttack2(): Either[String, DistanceMatrix] =
    Try(readFromArray(jsonStringAttack2.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJsoniterScalaAttack3(): Either[String, DistanceMatrix] =
    Try(readFromArray(jsonStringAttack3.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeCirceSuccess1(): Either[circe.Error, DistanceMatrix] =
    circe.parser.decode[DistanceMatrix](jsonString)

  @Benchmark
  def decodeCirceSuccess2(): Either[circe.Error, DistanceMatrix] =
    circe.parser.decode[DistanceMatrix](jsonStringCompact)

  @Benchmark
  def decodeCirceError(): Either[circe.Error, DistanceMatrix] =
    circe.parser.decode[DistanceMatrix](jsonStringErr)

  @Benchmark
  def decodeCirceAttack1(): Either[circe.Error, DistanceMatrix] =
    circe.parser.decode[DistanceMatrix](jsonStringAttack1)

  @Benchmark
  def decodeCirceAttack2(): Either[circe.Error, DistanceMatrix] =
    circe.parser.decode[DistanceMatrix](jsonStringAttack2)

  @Benchmark
  def decodeCirceAttack3(): Either[circe.Error, DistanceMatrix] =
    circe.parser.decode[DistanceMatrix](jsonStringAttack3)

  @Benchmark
  def decodePlaySuccess1(): Either[String, DistanceMatrix] =
    Try(Play.Json.parse(jsonString).as[DistanceMatrix]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodePlaySuccess2(): Either[String, DistanceMatrix] =
    Try(Play.Json.parse(jsonStringCompact).as[DistanceMatrix]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodePlayError(): Either[String, DistanceMatrix] =
    Try(Play.Json.parse(jsonStringErr).as[DistanceMatrix]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodePlayAttack1(): Either[String, DistanceMatrix] =
    Try(Play.Json.parse(jsonStringAttack1).as[DistanceMatrix]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodePlayAttack2(): Either[String, DistanceMatrix] =
    Try(Play.Json.parse(jsonStringAttack2).as[DistanceMatrix]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodePlayAttack3(): Either[String, DistanceMatrix] =
    Try(Play.Json.parse(jsonStringAttack3).as[DistanceMatrix]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJzonSuccess1(): Either[String, DistanceMatrix] =
    jzon.parser.decode[DistanceMatrix](jsonChars)

  @Benchmark
  def decodeJzonSuccess2(): Either[String, DistanceMatrix] =
    jzon.parser.decode[DistanceMatrix](jsonCharsCompact)

  @Benchmark
  def decodeJzonError(): Either[String, DistanceMatrix] =
    jzon.parser.decode[DistanceMatrix](jsonCharsErr)

  @Benchmark
  def decodeJzonAttack1(): Either[String, DistanceMatrix] =
    jzon.parser.decode[DistanceMatrix](jsonCharsAttack1)

  @Benchmark
  def decodeJzonAttack2(): Either[String, DistanceMatrix] =
    jzon.parser.decode[DistanceMatrix](jsonCharsAttack2)

  @Benchmark
  def decodeJzonAttack3(): Either[String, DistanceMatrix] =
    jzon.parser.decode[DistanceMatrix](jsonCharsAttack3)

}

object GoogleMapsAPIBenchmarks {
  implicit val codec: JsonValueCodec[DistanceMatrix] =
    JsonCodecMaker.make
}
