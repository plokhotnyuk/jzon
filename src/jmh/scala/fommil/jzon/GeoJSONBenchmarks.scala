package jzon

import java.nio.charset.StandardCharsets._
import java.util.concurrent.TimeUnit

import com.github.plokhotnyuk.jsoniter_scala.core._
import com.github.plokhotnyuk.jsoniter_scala.macros._
import io.circe
import jzon.GeoJSONBenchmarks._
import jzon.TestUtils._
//import jzon.data.geojson.generated._
import jzon.data.geojson.handrolled._
import org.openjdk.jmh.annotations._
import play.api.libs.{json => Play}

import scala.util.Try

// jmh:run GeoJSONBenchmarks

// see GoogleMapsAPIBenchmarks for profiling instructions

// set neoJmhYourkit in Jmh := Seq("sampling")
// jmh:run -i 1 -wi 0 -f0 -t1 -w0 -r60 GeoJSON.*JzonSuccess2
// jmh:run -i 1 -wi 0 -f1 -t1 -w0 -r60 -prof jmh.extras.Async GeoJSON.*JzonSuccess2
/*
desktop

[info] Benchmark                               Mode  Cnt      Score     Error  Units
[info] GeoJSONBenchmarks.decodeCirceError     thrpt    5  16744.316 ± 106.758  ops/s
[info] GeoJSONBenchmarks.decodeCirceSuccess1  thrpt    5  17173.384 ± 164.787  ops/s
[info] GeoJSONBenchmarks.decodeCirceSuccess2  thrpt    5  17294.231 ± 355.543  ops/s
[info] GeoJSONBenchmarks.decodeJzonError      thrpt    5  14720.385 ±  86.263  ops/s
[info] GeoJSONBenchmarks.decodeJzonSuccess1   thrpt    5  15843.260 ± 122.516  ops/s
[info] GeoJSONBenchmarks.decodeJzonSuccess2   thrpt    5   7304.689 ±  62.335  ops/s

with the handrolled variant

[info] GeoJSONBenchmarks.decodeJzonError      thrpt    5  32342.840 ± 283.188  ops/s
[info] GeoJSONBenchmarks.decodeJzonSuccess1   thrpt    5  34754.189 ± 261.134  ops/s
[info] GeoJSONBenchmarks.decodeJzonSuccess2   thrpt    5  34993.745 ± 219.417  ops/s

 XPS 13 laptop

[info] Benchmark                               Mode  Cnt      Score      Error  Units
[info] GeoJSONBenchmarks.decodeCirceError     thrpt    5  10096.294 ±  632.728  ops/s
[info] GeoJSONBenchmarks.decodeCirceSuccess1  thrpt    5  10466.730 ±  346.047  ops/s
[info] GeoJSONBenchmarks.decodeCirceSuccess2  thrpt    5  11423.303 ±  354.205  ops/s
[info] GeoJSONBenchmarks.decodeJzonError      thrpt    5  20787.146 ±  413.432  ops/s
[info] GeoJSONBenchmarks.decodeJzonSuccess1   thrpt    5  19543.595 ± 6669.057  ops/s
[info] GeoJSONBenchmarks.decodeJzonSuccess2   thrpt    5  21168.115 ± 2790.017  ops/s
[info] GeoJSONBenchmarks.decodePlayError      thrpt    5    728.928 ±  40.699  ops/s
[info] GeoJSONBenchmarks.decodePlaySuccess1   thrpt    5   7102.797 ± 157.847  ops/s
[info] GeoJSONBenchmarks.decodePlaySuccess2   thrpt    5   7165.676 ± 132.438  ops/s
 */

@State(Scope.Thread)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(value = 1, jvmArgs = Array(
  "-server",
  "-Xms2g",
  "-Xmx2g",
  "-XX:NewSize=1g",
  "-XX:MaxNewSize=1g",
  "-XX:InitialCodeCacheSize=512m",
  "-XX:ReservedCodeCacheSize=512m",
  "-XX:+UseParallelGC",
  "-XX:-UseAdaptiveSizePolicy",
  "-XX:MaxInlineLevel=18",
  "-XX:-UseBiasedLocking",
  "-XX:+AlwaysPreTouch",
  "-XX:+UseNUMA",
  "-XX:-UseAdaptiveNUMAChunkSizing"
))
class GeoJSONBenchmarks {
  var jsonString1, jsonString2, jsonStringErr: String = _
  var jsonChars1, jsonChars2, jsonCharsErr: CharSequence = _

  @Setup
  def setup(): Unit = {
    jsonString1 = getResourceAsString("che.geo.json")
    jsonChars1 = asChars(jsonString1)
    jsonString2 = getResourceAsString("che-2.geo.json")
    jsonChars2 = asChars(jsonString2)
    jsonStringErr = getResourceAsString("che-err.geo.json")
    jsonCharsErr = asChars(jsonStringErr)

    assert(decodeJsoniterScalaSuccess1() == decodeJzonSuccess1())
    assert(decodeJsoniterScalaSuccess2() == decodeJzonSuccess2())
    assert(decodeJsoniterScalaError().isLeft)

    assert(decodeCirceSuccess1() == decodeJzonSuccess1())
    assert(decodeCirceSuccess2() == decodeJzonSuccess2())
    assert(decodeCirceError().isLeft)

    // these are failing because of a bug in play-json, but they succeed
    // assert(decodeCirceSuccess1() == decodePlaySuccess1(), decodePlaySuccess1().toString)
    // assert(decodeCirceSuccess2() == decodePlaySuccess2())
    assert(decodePlaySuccess1().isRight)
    assert(decodePlaySuccess2().isRight)
    assert(decodePlayError().isLeft)

    assert(decodeJzonError().isLeft)
  }

  @Benchmark
  def decodeJsoniterScalaSuccess1(): Either[String, GeoJSON] =
    Try(readFromArray(jsonString1.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJsoniterScalaSuccess2(): Either[String, GeoJSON] =
    Try(readFromArray(jsonString2.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJsoniterScalaError(): Either[String, GeoJSON] =
    Try(readFromArray(jsonStringErr.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeCirceSuccess1(): Either[circe.Error, GeoJSON] =
    circe.parser.decode[GeoJSON](jsonString1)

  @Benchmark
  def decodeCirceSuccess2(): Either[circe.Error, GeoJSON] =
    circe.parser.decode[GeoJSON](jsonString2)

  @Benchmark
  def decodeCirceError(): Either[circe.Error, GeoJSON] =
    circe.parser.decode[GeoJSON](jsonStringErr)

  @Benchmark
  def decodePlaySuccess1(): Either[String, GeoJSON] =
    Try(Play.Json.parse(jsonString1).as[GeoJSON]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodePlaySuccess2(): Either[String, GeoJSON] =
    Try(Play.Json.parse(jsonString2).as[GeoJSON]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodePlayError(): Either[String, GeoJSON] =
    Try(Play.Json.parse(jsonStringErr).as[GeoJSON]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJzonSuccess1(): Either[String, GeoJSON] =
    jzon.parser.decode[GeoJSON](jsonChars1)

  @Benchmark
  def decodeJzonSuccess2(): Either[String, GeoJSON] =
    jzon.parser.decode[GeoJSON](jsonChars2)

  @Benchmark
  def decodeJzonError(): Either[String, GeoJSON] =
    jzon.parser.decode[GeoJSON](jsonCharsErr)

}

object GeoJSONBenchmarks {
  implicit val codec: JsonValueCodec[GeoJSON] =
    JsonCodecMaker.make(CodecMakerConfig.withAllowRecursiveTypes(true).withRequireDiscriminatorFirst(false))
}
