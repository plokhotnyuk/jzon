package jzon

import java.nio.charset.StandardCharsets.UTF_8
import java.util.concurrent.TimeUnit

import com.github.plokhotnyuk.jsoniter_scala.core._
import com.github.plokhotnyuk.jsoniter_scala.macros._
import io.circe
import jzon.SyntheticBenchmarks._
import jzon.TestUtils._
import org.openjdk.jmh.annotations._
import play.api.libs.{json => Play}

import scala.util.Try

// jmh:run SyntheticBenchmarks
//
// see GoogleMapsAPIBenchmarks for profiling instructions

/*
desktop

[info] Benchmark                                Mode  Cnt       Score     Error  Units
[info] SyntheticBenchmarks.decodeCirceSuccess  thrpt    5   39211.437 ± 187.687  ops/s
[info] SyntheticBenchmarks.decodeJzonSuccess   thrpt    5  115834.917 ± 883.412  ops/s

XPS 13

[info] Benchmark                                Mode  Cnt      Score      Error  Units
[info] SyntheticBenchmarks.decodeCirceSuccess  thrpt    5  23230.012 ±  642.395  ops/s
[info] SyntheticBenchmarks.decodeJzonSuccess   thrpt    5  71044.818 ± 1405.865  ops/s
[info] SyntheticBenchmarks.decodePlaySuccess   thrpt    5  10494.717 ±  502.218  ops/s

 */

final case class Nested(n: Option[Nested])
object Nested {
  implicit lazy val jzonDecoder: jzon.Decoder[Nested] =
    jzon.MagnoliaDecoder.gen

  implicit val customConfig: circe.generic.extras.Configuration =
    circe.generic.extras.Configuration.default
      .copy(discriminator = Some("type"))
  implicit lazy val circeDecoder: circe.Decoder[Nested] =
    circe.generic.extras.semiauto.deriveConfiguredDecoder[Nested]
  implicit lazy val circeEncoder: circe.Encoder[Nested] =
    circe.generic.extras.semiauto.deriveConfiguredEncoder[Nested]

  implicit lazy val playFormatter: Play.Format[Nested] =
    Play.Json.format[Nested]

}

@State(Scope.Thread)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(value = 1, jvmArgs = Array(
  "-server",
  "-Xms2g",
  "-Xmx2g",
  "-XX:NewSize=1g",
  "-XX:MaxNewSize=1g",
  "-XX:InitialCodeCacheSize=512m",
  "-XX:ReservedCodeCacheSize=512m",
  "-XX:+UseParallelGC",
  "-XX:-UseAdaptiveSizePolicy",
  "-XX:MaxInlineLevel=18",
  "-XX:-UseBiasedLocking",
  "-XX:+AlwaysPreTouch",
  "-XX:+UseNUMA",
  "-XX:-UseAdaptiveNUMAChunkSizing"
))
class SyntheticBenchmarks {
  //@Param(Array("100", "1000"))
  var size: Int = 500
  var jsonString: String = _
  var jsonChars: CharSequence = _

  @Setup
  def setup(): Unit = {
    val obj = 1.to(size).foldLeft(Nested(None))((n, _) => Nested(Some(n)))

    jsonString = {
      import circe.syntax._

      obj.asJson.noSpaces
    }
    jsonChars = asChars(jsonString)

    assert(decodeJsoniterScalaSuccess() == decodeJzonSuccess())

    assert(decodeCirceSuccess() == decodeJzonSuccess())

    assert(decodePlaySuccess() == decodeJzonSuccess())
  }

  @Benchmark
  def decodeJsoniterScalaSuccess(): Either[String, Nested] =
    Try(readFromArray(jsonString.getBytes(UTF_8))).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeCirceSuccess(): Either[circe.Error, Nested] =
    circe.parser.decode[Nested](jsonString)

  @Benchmark
  def decodePlaySuccess(): Either[String, Nested] =
    Try(Play.Json.parse(jsonString).as[Nested]).fold(t => Left(t.toString), Right.apply)

  @Benchmark
  def decodeJzonSuccess(): Either[String, Nested] =
    jzon.parser.decode[Nested](jsonChars)

}

object SyntheticBenchmarks {
  implicit val codec: JsonValueCodec[Nested] =
    JsonCodecMaker.make(CodecMakerConfig.withAllowRecursiveTypes(true))
}